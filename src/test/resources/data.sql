DROP TABLE IF EXISTS principal_roles;
DROP TABLE IF EXISTS role;

CREATE TABLE role (
    id   SERIAL  NOT NULL CONSTRAINT role_pk PRIMARY KEY,
    name VARCHAR NOT NULL
);

CREATE TABLE principal_roles (
    principal_name VARCHAR NOT NULL,
    role_id        INTEGER NOT NULL CONSTRAINT principal_roles_role_id_fk REFERENCES role,
    CONSTRAINT principal_roles_pk PRIMARY KEY (principal_name, role_id)
);

INSERT INTO role (id, name) VALUES
    (1, 'admin'),
    (2, 'user'),
    (3, 'guest');

INSERT INTO principal_roles (principal_name, role_id) VALUES
    ('ADMIN', 1),
    ('admin', 2),
    ('user', 2),
    ('guest', 3);
