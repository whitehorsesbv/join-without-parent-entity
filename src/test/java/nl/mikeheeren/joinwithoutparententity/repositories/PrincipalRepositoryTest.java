package nl.mikeheeren.joinwithoutparententity.repositories;

import nl.mikeheeren.joinwithoutparententity.model.Principal;
import nl.mikeheeren.joinwithoutparententity.model.Role;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.jdbc.Sql;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

@SpringBootTest
@Sql("/data.sql")
class PrincipalRepositoryTest {

    private static final Role ADMIN, USER, GUEST;

    static {
        ADMIN = new Role();
        ADMIN.setId(1);
        ADMIN.setName("admin");

        USER = new Role();
        USER.setId(2);
        USER.setName("user");

        GUEST = new Role();
        GUEST.setId(3);
        GUEST.setName("guest");
    }

    @Autowired
    private PrincipalRepository principalRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    void testFindAllPrincipals() {
        // When
        List<Principal> result = principalRepository.findAllPrincipals();

        // Then
        assertEqualsList(List.of(new Principal("admin", List.of(ADMIN, USER)),
                new Principal("user", List.of(USER)),
                new Principal("guest", List.of(GUEST))), result);
    }

    @Test
    void testFindPrincipalById() {
        // When
        Optional<Principal> result = principalRepository.findPrincipalById("ADMIN");

        // Then
        assertTrue(result.isPresent());
        assertEquals("admin", result.get().getPrincipalName().toLowerCase());
        assertEqualsList(List.of(ADMIN, USER), result.get().getRoles());

    }

    @Test
    void testFindPrincipalByIdCaseInsensitive() {
        // When
        Optional<Principal> result = principalRepository.findPrincipalById("USER");

        // Then
        assertTrue(result.isPresent());
        assertEquals("user", result.get().getPrincipalName());
        assertEqualsList(List.of(USER), result.get().getRoles());
    }

    @Test
    void testFindPrincipalByIdNotFound() {
        // When
        Optional<Principal> result = principalRepository.findPrincipalById("non-existing");

        // Then
        assertFalse(result.isPresent());
    }

    @Test
    void testSaveNew() {
        // Given
        String principalName = "new-principal";
        Principal principal = new Principal(principalName, List.of(USER, GUEST));
        assertNoRolesInDatabase(principalName);

        // When
        Principal result = principalRepository.save(principal);

        // Then
        assertEquals(principal, result);
        assertRolesInDatabase(principalName, USER, GUEST);
    }

    @Test
    void testSaveAddRoleToExisting() {
        // Given
        String principalName = "USER";
        Principal principal = new Principal(principalName, List.of(ADMIN, USER));
        assertRolesInDatabase(principalName, USER);

        // When
        Principal result = principalRepository.save(principal);

        // Then
        assertEquals(principal, result);
        assertRolesInDatabase(principalName, ADMIN, USER);
    }

    @Test
    void testSaveRemoveRoleFromExisting() {
        // Given
        String principalName = "admin";
        Principal principal = new Principal(principalName, List.of(ADMIN));
        assertRolesInDatabase(principalName, ADMIN, USER);

        // When
        Principal result = principalRepository.save(principal);

        // Then
        assertEquals(principal, result);
        assertRolesInDatabase(principalName, ADMIN);
    }

    @Test
    void testSaveReplaceRoleOnExisting() {
        // Given
        String principalName = "guest";
        Principal principal = new Principal(principalName, List.of(USER));
        assertRolesInDatabase(principalName, GUEST);

        // When
        Principal result = principalRepository.save(principal);

        // Then
        assertEquals(principal, result);
        assertRolesInDatabase(principalName, USER);
    }

    @Test
    void testSaveRemoveAllRolesFromExisting() {
        // Given
        String principalName = "ADMIN";
        Principal principal = new Principal(principalName, Collections.emptyList());
        assertRolesInDatabase(principalName, ADMIN, USER);

        try {
            // When
            principalRepository.save(principal);

            // Then
            fail("DataIntegrityViolationException expected");
        } catch (DataIntegrityViolationException ex) {
            // Then
            assertRolesInDatabase(principalName, ADMIN, USER);
        }
    }

    private void assertNoRolesInDatabase(String principalName) {
        assertRolesInDatabase(principalName);
    }

    private void assertRolesInDatabase(String principalName, Role... expectedRoles) {
        List<Integer> roleIds = jdbcTemplate.queryForList(
                String.format("SELECT role_id FROM principal_roles WHERE LOWER(principal_name) = '%s'", principalName.toLowerCase()),
                Integer.class);
        assertEqualsList(Stream.of(expectedRoles).map(Role::getId).collect(Collectors.toList()), roleIds);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private void assertEqualsList(List expected, List actual) {
        assertTrue(expected.size() == actual.size() && expected.containsAll(actual) && actual.containsAll(expected),
                () -> String.format("\nExpected :%s\nActual   :%s", expected, actual));
    }

}
