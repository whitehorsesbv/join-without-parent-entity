package nl.mikeheeren.joinwithoutparententity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JoinWithoutParentEntityApplication {

	public static void main(String[] args) {
		SpringApplication.run(JoinWithoutParentEntityApplication.class, args);
	}

}
