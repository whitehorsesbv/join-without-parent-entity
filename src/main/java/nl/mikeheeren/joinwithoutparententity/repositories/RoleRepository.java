package nl.mikeheeren.joinwithoutparententity.repositories;

import nl.mikeheeren.joinwithoutparententity.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
}
