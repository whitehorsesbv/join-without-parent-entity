package nl.mikeheeren.joinwithoutparententity.repositories;

import nl.mikeheeren.joinwithoutparententity.model.Principal;
import nl.mikeheeren.joinwithoutparententity.model.PrincipalRoles;
import nl.mikeheeren.joinwithoutparententity.model.Role;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public interface PrincipalRepository extends JpaRepository<PrincipalRoles, String> {

    default List<Principal> findAllPrincipals() {
        Map<String, List<PrincipalRoles>> caseInsensitiveBundle = findAll().stream()
                .collect(Collectors.groupingBy(principalRoles -> principalRoles.getPrincipalName().toLowerCase(), Collectors.toList()));

        return caseInsensitiveBundle.values().stream()
                .map(PrincipalRepository::toPrincipal)
                .collect(Collectors.toList());
    }

    default Optional<Principal> findPrincipalById(String principalName) {
        List<PrincipalRoles> principalRoles = findAllByPrincipalName(principalName);
        if (principalRoles.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(toPrincipal(principalRoles));
    }

    @Transactional
    default Principal save(Principal principal) {
        String principalName = principal.getPrincipalName();
        List<Role> roles = principal.getRoles();
        if (roles == null || roles.isEmpty()) {
            throw new DataIntegrityViolationException("A principal should always contain at least 1 role.");
        }
        List<PrincipalRoles> principalRoles = roles.stream()
                .map(role -> {
                    var pr = new PrincipalRoles();
                    pr.setPrincipalName(principalName);
                    pr.setRole(role);
                    return pr;
                })
                .collect(Collectors.toList());
        // Delete existing records
        deleteAllByPrincipalName(principalName);
        // (Re)insert roles
        List<PrincipalRoles> savedPrincipalRoles = saveAll(principalRoles);
        return toPrincipal(savedPrincipalRoles);
    }

    @Query("SELECT principalRoles FROM PrincipalRoles principalRoles WHERE LOWER(principalRoles.principalName) = LOWER(:principalName)")
    List<PrincipalRoles> findAllByPrincipalName(@Param("principalName") String principalName);

    @Modifying
    @Transactional
    @Query("DELETE FROM PrincipalRoles principalRoles WHERE LOWER(principalRoles.principalName) = LOWER(:principalName)")
    void deleteAllByPrincipalName(@Param("principalName") String principalName);

    private static Principal toPrincipal(List<PrincipalRoles> principalRoles) {
        String principalName = principalRoles.get(0).getPrincipalName();
        List<Role> roles = principalRoles.stream().map(PrincipalRoles::getRole).collect(Collectors.toList());
        return new Principal(principalName, roles);
    }

}
