package nl.mikeheeren.joinwithoutparententity.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "principal_roles")
@IdClass(PrincipalRoles.PrimaryKey.class)
public class PrincipalRoles {

    @Id
    @Column(name = "principal_name")
    private String principalName;

    @Id
    @Column(name = "role_id")
    @SuppressWarnings("unused") // Required for composite ID
    private int roleId;

    @ManyToOne
    @JoinColumn(name = "role_id", insertable = false, updatable = false)
    private Role role;

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
        this.roleId = role == null ? 0 : role.getId();
    }

    @SuppressWarnings({"java:S1068", "unused"}) // Private fields required for composite ID
    static class PrimaryKey implements Serializable {

        private static final long serialVersionUID = 1L;

        private String principalName;
        private int roleId;

    }

}
