package nl.mikeheeren.joinwithoutparententity.model;

import java.util.List;
import java.util.Objects;

public class Principal {

    private final String principalName;
    private final List<Role> roles;

    public Principal(String principalName, List<Role> roles) {
        this.principalName = principalName;
        this.roles = roles;
    }

    public String getPrincipalName() {
        return principalName;
    }

    public List<Role> getRoles() {
        return roles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o == null || getClass() != o.getClass()) {
            return false;
        } else {
            var principal = (Principal) o;
            return Objects.equals(getLowerPrincipalName(), principal.getLowerPrincipalName()) &&
                    Objects.equals(roles, principal.roles);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLowerPrincipalName(), roles);
    }

    @Override
    public String toString() {
        return String.format("Principal[name='%s', roles=%s]", principalName, roles);
    }

    private String getLowerPrincipalName() {
        return principalName == null ? null : principalName.toLowerCase();
    }

}
