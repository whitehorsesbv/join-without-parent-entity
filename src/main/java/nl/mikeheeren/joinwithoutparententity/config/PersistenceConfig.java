package nl.mikeheeren.joinwithoutparententity.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "nl.mikeheeren.joinwithoutparententity.repositories")
public class PersistenceConfig {

}
